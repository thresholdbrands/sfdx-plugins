import { flags, SfdxCommand } from "@salesforce/command";
import { Messages } from "@salesforce/core";
import { AnyJson } from "@salesforce/ts-types";
import * as tbCore from "@thresholdbrands/sfdx-core"

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages(
  "@thresholdbrands/sfdx-plugins",
  "buildManifest"
);

export default class BuildManifest extends SfdxCommand {
  public static description = messages.getMessage("commandDescription");

  public static examples = [
    `$ sfdx thresholdbrands:source:buildManifest --targetusername|-u myOrg@example.com --apiversion 46.0 -o manifest/package.xml -x PermissionSet,Profile
  Retrieving metadata...
  Building package.xml...
  Retrieving source...
  Finished!
  `
  ];

  protected static flagsConfig = {
    excludetypes: flags.string({
      char: "x",
      description: messages.getMessage("excludeTypesFlagDescription"),
      required: false,
      default: ""
    }),
    outputfilename: flags.filepath({
      char: "o",
      description: messages.getMessage("outputFileNameFlagDescription"),
      required: false,
      default: "manifest/package.xml"
    })
  };

  // Comment this out if your command does not require an org username
  protected static requiresUsername = true;

  // Comment this out if your command does not support a hub org username
  protected static supportsDevhubUsername = false;

  // Set this to true if your command requires a project workspace; 'requiresProject' is false by default
  protected static requiresProject = true;

  public async run(): Promise<AnyJson> {
    const options: tbCore.ManifestBuilderOptions = new tbCore.ManifestBuilderOptions();
    const builder: tbCore.ManifestBuilder = new tbCore.ManifestBuilder(options, this.org, this.logMessage);
    await builder.buildManifest();
    return null;
  }

  // logging function -- arrow function preserves 'this'
  public logMessage = (msg) => {
    this.ux.log(msg);
  }
}
