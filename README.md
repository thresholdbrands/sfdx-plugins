# sfdx-plugins

A set of plugins for the Salesforce CLI built by the Threshold Brands team for internal use, but shared with the Salesforce community. No guarantees on support or updates.

## Install

`sfdx plugins:install @thresholdbrands/sfdx-plugins`

You'll be prompted that this, like any plugin, is not officially code-signed by Salesforce. If that's annoying, you can [whitelist it](https://developer.salesforce.com/blogs/2017/10/salesforce-dx-cli-plugin-update.html)

## Contribute

PRs are welcome!

## Usage

<!-- usage -->
```sh-session
$ npm install -g @thresholdbrands/sfdx-plugins
$ sfdx COMMAND
running command...
$ sfdx (-v|--version|version)
@thresholdbrands/sfdx-plugins/2.0.5 darwin-x64 node-v14.4.0
$ sfdx --help [COMMAND]
USAGE
  $ sfdx COMMAND
...
```
<!-- usagestop -->

## Commands

<!-- commands -->
* [`sfdx zervice:source:buildManifest [-x <string>] [-o <filepath>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-zervicesourcebuildmanifest--x-string--o-filepath--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx zervice:source:buildManifest [-x <string>] [-o <filepath>] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

Retrieves all supported Metadata type and creates/updates the XML manifest file

```
USAGE
  $ sfdx zervice:source:buildManifest [-x <string>] [-o <filepath>] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -o, --outputfilename=outputfilename                                               [default: manifest/package.xml] The
                                                                                    path and filename to output the
                                                                                    resulting manifest file

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  -x, --excludetypes=excludetypes                                                   A comma-separated list of Metadata
                                                                                    Types to exclude from the manifest

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLE
  $ sfdx thresholdbrands:source:buildManifest --targetusername|-u myOrg@example.com --apiversion 46.0 -o 
  manifest/package.xml -x PermissionSet,Profile
     Retrieving metadata...
     Building package.xml...
     Retrieving source...
     Finished!
```
<!-- commandsstop -->
