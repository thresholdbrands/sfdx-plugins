# 2.0.5 - Jul 29, 2021

- Renamed package from `@zervice/zervice-sfdx-core` to `@thresholdbrands/sfdx-core` to reflect new company name
# 2.0.4 - August 10, 2020

- Updated dependencies, switched back to npm from yarn to fix dependency version conflict

# 2.0.3 - June 15, 2020

- Downgraded Node requirement to fix install failure

# 2.0.2 - June 15, 2020

- Fix for yarn

# 2.0.1 - June 15, 2020

- Updated library to match core

# 2.0.0 - June 15, 2020

- Refactored command logic into new, separate `@zervice/zervice-sfdx-core` package

# 1.0.2 - July 29, 2019

- General cleanup

# 1.0.1 - July 29, 2019

## Fixed

- Updated package name on messages load command to fix plugin not loading

# 1.0.0 - July 29, 2019

- Inital release
- Adds `zervice:source:buildManifest` command